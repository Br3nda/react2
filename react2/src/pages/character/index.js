import React, {useContext, useEffect, useRef, Suspense} from 'react';
import User from '../../components/User';
// import Klasa from '../../components/Klasa';
// import Oponent from '../../components/Oponent'
import { UserStatsProvider, OponentStatsProvider, GameState } from '../../context';
import Form from '../../components/Form';
import Header from '../../components/Header';
import MyTest from '../../components/MyTest';
// import TableButton from '../../components/TableRow/tableButton';
// import TableRow from '../../components/TableRow';

const Character = () => {
  return ( 
    <div className="col-12">
        {/* <Profiler id="UserStatsProvider" onRender={callback}> */}
             <UserStatsProvider>
                     <User />
                  </UserStatsProvider>
                  {/* </Profiler> */}
                  <OponentStatsProvider>
                  {/* <Profiler id="User" onRender={callback}> */}
                     <User alive />
                  {/* </Profiler> */}
          </OponentStatsProvider>
          </div>
   );
}
 
export default Character;