import React, {useContext, useEffect, useRef, Suspense} from 'react';
// import User from '../../components/User';
// import Klasa from '../../components/Klasa';
// import Oponent from '../../components/Oponent'
import { UserStatsProvider, OponentStatsProvider, GameState } from '../../context';
import Form from '../../components/Form';
import Header from '../../components/Header';
import MyTest from '../../components/MyTest';
// import TableButton from '../../components/TableRow/tableButton';
// import TableRow from '../../components/TableRow';

const Game = () => {
  const { isName, setName } = useContext(GameState);
  
  useEffect(() => {
    const getName = localStorage.getItem('name');
    if (Boolean(getName)) { setName(true) };
  }, [isName]);

  const refPo = useRef(null);
  const refL = useRef(null);
  const refD = useRef(null);
  const refPu = useRef(null);
  const refG = useRef(null);

  return ( 
    <div>
        {isName ?  
           
               (<div>
                  <Header {...{ refPo, refL, refD, refPu, refG }} />
                  <div className="col-12 destination" ref={refPo}>Polana</div>
                  <div className="col-12 destination" ref={refL}>Las</div>
                  <div className="col-12 destination" ref={refD}>Dżungla</div>
                  <div className="col-12 destination" ref={refPu}>Pustynia</div>
                  <div className="col-12 destination" ref={refG}>Góry</div>
                  <div className="game__wrapper">
                 
               </div>
            </div>
            ) : <Form/>
      }</div>
   );
}
 
export default Game;