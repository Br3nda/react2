import React, { useEffect, useState } from 'react';
import TableButton from '../TableRow/tableButton';

import './style.css';

const TableRow = props => {
   const [str, randomStr] = useState(20);
   const [hp, randomHp] = useState(22);
   const [speed, randomSpeed] = useState(2);
   const dmg = str * speed;
   const oponentStats = { str, hp, speed, dmg, ...props };
   // const [dmg, randomStr] = useState(10);
   // const [str, randomStr] = useState(10);

   const handleChangeOponent = (el) => {
      randomStr(Math.floor(Math.random() * 20+1));
      randomHp(Math.floor(Math.random() * 30+1));
      randomSpeed(Math.floor(Math.random() * 5+1));
   }

   return (
      <>
         {props.render({...oponentStats})}
         {props.oponent && <TableButton onClick={el => handleChangeOponent(el)} />}
      </>
   )
}

export default TableRow;