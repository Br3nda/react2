import React, {useContext, useState} from 'react';
import { GameState } from '../../context';

const Form = () => {
   // let { validForm, randomHP, randomSpeed, changeValidForm, } = useContext(CharStats);
   const [name, addName] = useState('');
   const { setName } = useContext(GameState);

   const handleSubmitForm = e => {
      e.preventDefault();
      const inputName = document.getElementById('name');
      if (inputName.value) {
         setName(true);
         localStorage.setItem('name', name);
         // randomStr(Math.floor(Math.random() * 10));
         // randomHP(Math.floor(Math.random() * 100));
         // randomSpeed(Math.floor(Math.random() * 20));
         // changeValidForm(!validForm);
      }
   }

   return ( 
      <form onSubmit={handleSubmitForm}>
         <label htmlFor='name'>Wybierz imię bohatera</label>
         <input type="text" id='name' value={name} onChange={e => addName(e.target.value)}/>
         <button>Potwierdz bohatera</button>
      </form>
    );
}
 
export default Form;