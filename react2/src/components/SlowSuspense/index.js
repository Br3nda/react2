import React, { Suspense, useEffect, useState } from 'react';

const SlowSuspense = ({ children, fallback = null }) => { 
  const [show, setShow] = useState(true);
  useEffect(() => {
    setTimeout(() => setShow(false), 2000);
  }, []);

  return (
    <div>
      {show && fallback}
      <Suspense fallback={fallback}>
        {!show && children}
      </Suspense>
    </div>
    );
}
 
export default SlowSuspense;