import React, { useState } from 'react';
// import MyTest from '.';
// import MyText from './myText';

const MyMouse = props => {
  const [mousePos, setMousePos] = useState({ 'x': 0, 'y': 0 });

  const handleMouseMove = el => {
    setMousePos({ 'x': el.clientX, 'y': el.clientY });
  }
  return ( 
    <div className="col-12 destination" onMouseMove={el=>handleMouseMove(el)}>
      <h2>Rusz myszką</h2>
      {props.render(mousePos)}
      {/* <MyText mousePos={mousePos}/> */}
    </div> 
    
   );
}
 
export default MyMouse;