import React from 'react';
import MyMouse from './myMouse.js';
import MyText from './myText.js';

const MyTest = props => {
  return ( 
    <div>
      <MyMouse render={mousePos => (
        <MyText mousePos={mousePos }/>
      ) }/>
    </div> 
    
   );
}
 
export default MyTest;