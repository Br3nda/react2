import React from 'react';

const ChangeOponent = props => {

  
  return ( 
    <>
      <table>
            <tbody>
               <tr><td>Nick: {props.name}</td></tr>
               <tr><td>Siła: {props.str}</td></tr>
               <tr><td>Życie: {props.hp}</td></tr>
               <tr><td>Prędkość ataku: {props.speed}</td></tr>
               <tr><td>Obrażenia: {props.dmg}</td></tr>
            </tbody>
         </table>
         <div className="experience-lvl__wrapper">
            <div>
               <p>Poziom: {props.lvl}</p>
            </div>
            {!props.oponent ?
            <div className="experience-bar"><span>{props.exp}/300</span>
               <div className="experience-bar__gained"></div>
            </div> : null}
            
         </div>
    </>
   );
}
 
export default ChangeOponent;