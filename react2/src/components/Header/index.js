import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

const Header = ({ refPo, refL, refD, refPu, refG }) => {

  const navList = [
    { 'name': "Polana", 'ref': refPo },
    { 'name': "Las", 'ref': refL },
    { 'name': "Dżungla", 'ref': refD },
    { 'name': "Pustynia", 'ref': refPu },
    { 'name': "Góry", 'ref': refG }
  ];
  
  const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop-30);

  return ( 
    <div className="nav">
      {/* <ul>
        {
          navList.map(el => {
            return <li key={el.name} onClick={() => scrollToRef(el.ref)}>{el.name}</li>
          })
        }
      </ul> */}
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/user">Postać</Link>
        </li>
        <li>
          <Link to="/game">Świat gry</Link>
        </li>
      </ul>
    </div>
   );
}
 
export default Header;