import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Homepage from './pages/homepage';
import { GameApp } from './context';
import Header from './components/Header';
import User from './components/User';
import SlowSuspense from './components/SlowSuspense';
import Spinner from './components/Spinner';
import Game from './pages/game';
// import Character from './pages/character';
const Character = React.lazy(() => import('./pages/character'));

function App() {

  // localStorage.setItem('name','Demon');
  return (
    <Router>
      <GameApp>
    <div className="game__wrapper">
        <Header />
        <Switch>
          <Route exact path='/' component={Homepage}/>
          <Route exact path='/game' component={Game}/>
          <Route path='/game/one' component={Homepage}/>
          <Route path='/user'>
            <SlowSuspense fallback={<Spinner />}>
              <Character/>
            </SlowSuspense>
          </Route>
        </Switch>
      </div>
      </GameApp>
      </Router>
  );
}

export default App;
