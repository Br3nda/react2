import React, { createContext, useState } from 'react';

export const GameState = createContext({
   isName: false,
   setName: item => { }
});

export const GameApp = ({ children }) => {
   const [isName, setName] = useState(false);

   return (
      <GameState.Provider
         value={{
            isName,
            setName
         }}>
         {children}
      </GameState.Provider>
   )
};

export const CharStats = createContext({
   name: 'Demon',
   str: 0,
   hp: 0,
   speed: 0,
   dmg: 10,
   lvl: 1,
   exp:0,
   // validForm: true,
   setName: name => { },
   addStr: item => { },
   // randomStr: str => { },
   // randomHP: hp => { },
   // randomSpeed: speed => { },
   // changeValidForm: form => { },
});

export const UserStatsProvider = ({ children }) => {

   const [name, setName] = useState('');
   // const [str, randomStr] = useState('');
   const [str, addStr] = useState(10);
   const hp = 10;
   const speed = 2;
   const dmg = speed * str;
   const lvl = 1;
   const exp = 0;
   const oponent = false;
   // const [validForm, changeValidForm] = useState(true);

   return (
      <CharStats.Provider
         value={{
            name,
            str,
            hp,
            speed,
            dmg,
            lvl,
            exp,
            oponent,
            // validForm,
            setName,
            addStr,
            // randomHP,
            // randomSpeed,
            // changeValidForm,
         }}
      >
         {children}
      </CharStats.Provider>
   )

}

export const OponentStatsProvider = ({ children }) => {
   const name = 'Oponent';
   const str = 10;
   const hp = 12;
   const speed = 2;
   const dmg = speed * str;
   const lvl = 1;
   const oponent = true;

   return (
      <CharStats.Provider
         value={{
            name,
            str,
            hp,
            speed,
            dmg,
            lvl,
            oponent
         }}
      >
         {children}
      </CharStats.Provider>
   )

}