import React, { useEffect, useCallback, useState } from 'react';

const CallBackResult = ({ elPos, reverse, color }) => {
  
  const testFunction = useCallback(() => {
    console.log(color);

    
  }, [reverse]);
  
  // const testFunction = () => {
  //   // console.log(elPos);
  // }

  useEffect(() => {
    testFunction();
    console.log('test function changed');
  }, [testFunction, reverse]);

  return ( 
    <div>
      <p>top: {elPos.top} px</p>
      <p>left: {elPos.left} px</p>
      <p>{color.color1}  {color.color2 }</p>
      <button onClick={()=>testFunction()}>check function</button>
    </div>
   );
}
 
export default CallBackResult;