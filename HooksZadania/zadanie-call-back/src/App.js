import React, { useEffect, useRef, useState } from 'react';
import './App.css';
import styled from 'styled-components';
import CallBackResult from './CallBackResult';

const MyCircle = styled.div`
position:absolute;
top:${props => props.top + 'px'};
left:${props => props.left + 'px'};
background-color: ${props=>props.color};
width:20px;
height:20px;
border-radius:50%;
display: flex;
text-align:center;
align-items:center;
justify-content:center;
transition:all 0.5s;
`;

function App() {
  const colors = [
    {color1:'red', color2:'black'},
    {color1:'yellow', color2:'red'},
    {color1:'pink', color2:'yellow'},
    {color1:'red', color2:'blue'},
    {color1:'green', color2:'red'},
    {color1:'blue', color2:'orange'},
    {color1:'royalblue', color2:'black'},
    {color1:'black', color2:'gray'},
    {color1:'orange', color2:'gray'},
    {color1:'gray', color2:'green'},
  ]
  const [elPos, changeElPos] = useState({ top: 50, left: 20 });
  let [direction, setDirection] = useState('');
  const [color, setColor] = useState({ color1: 'black', color2: '' });
  let [coloredCircle, setColorCircle] = useState('');
  const ref = useRef(null);
  // let setColoredCircle = color.color1;
  const maxLeft = 500;

  useEffect(() => {
    // if()
    changeColor();
    // handleStartMoveCircle();
    // changeColorInTheMiddle();
    console.log(color);
    // console.log(coloredCircle);
    
    // console.log(elPos.top);
    console.log(ref.current);
  }, [direction]);
  
  
  const handleStartMoveCircle = () => {
    setDirection(direction = 'right');
    // changeColor();
    
    const topMin = 0;
    const topMax = 100;
    const leftMin = 10;
    const leftMax = 50;
    // clearInterval(interval)
    

    setInterval(() => {
      let top = Math.floor(Math.random() * (topMax - topMin)) + topMin;
      let left = Math.floor(Math.random() * (leftMax - leftMin)) + leftMin;

      

      direction === 'right' ? moveRight(top, left) : moveLeft(top, left);
      changeDirection(top);
      changeColorInTheMiddle();
      // console.log(color);
      // changeColor();
      // handleStartMoveCircle();
    }, 500);
  }
  

  const moveRight = (otherTop, otherLeft) => {
    changeElPos({ top: otherTop, left: elPos.left = elPos.left + otherLeft });
  };

  const moveLeft = (otherTop, otherLeft) => {
    changeElPos({ top: otherTop, left: elPos.left = elPos.left - otherLeft });
  };

  const changeDirection = top => {
    
    if (elPos.left <= 0) {
      changeElPos({ top: top, left: 0 });
      setDirection(direction = 'right');
    } else if (elPos.left >= maxLeft) {
      changeElPos({ top: top, left: maxLeft });
      setDirection(direction = 'left');
    };
  };

  const randomColor = () => {
    let colorsNumber = Math.floor(Math.random() * (colors.length - 1));
    let randomColoredCircle = colors[colorsNumber];

    return randomColoredCircle;
  }

  const changeColor = () => {
    const randomColors = randomColor();
    setColor({ color1: randomColors.color1, color2: randomColors.color2 });
    setColorCircle(color.color1);
    // coloredCircle = color.color1;
    // console.log(color)
    // console.log(color.color1, color.color2);
  };

  const changeColorInTheMiddle = () => {
    // console.log(direction === 'right' && elPos.left>= maxLeft/2);
    if ((direction === 'right' && elPos.left >= maxLeft / 2) || (direction === 'left' && elPos.left <= maxLeft / 2)) {
      setColorCircle(coloredCircle = color.color2);
      // setColoredCircle = color.color2;
    }
    // console.log(color);
  }

  
  return (
    <div className="App">
      <div className='square'>
        <MyCircle top={elPos.top} left={elPos.left} color={coloredCircle} ref={ref}/>
      </div>
      <button onClick={()=>handleStartMoveCircle()}>Start</button>
      <CallBackResult elPos={elPos} reverse={direction} color={color}/>
    </div>
  );
}

export default App;
