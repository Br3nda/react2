import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import RectangleResult from './RectangleResult';

import './App.css'

const MyRectangle = styled.div`
width:${props => props.width + 'px'};
height:${props => props.height + 'px'};
background-color:black;
display: flex;
text-align:center;
align-items:center;
justify-content:center;
transition:all 0.5s;
`;

const Rectangle = () => {
  const [size, setSize] = useState({ width: 103, height: 120 });
  const ref = useRef(null);
  useEffect(() => {
    setSize({ width: ref.current.clientWidth, height: ref.current.clientHeight });
    // console.log(ref.current.clientWidth);
  },[])
  // const ref = React.createRef();

  const handleResize = () => {
    const max = 500;
    const min = 100;
    let width = Math.floor(Math.random() * (max - min)) + min;
    let height = Math.floor(Math.random() * (max - min)) + min;

    setSize({ width: width, height: height });
  }
    return (
      <div>
        <MyRectangle
          ref={ref}
          width={size.width}
          height={size.height}
        >
          <RectangleResult {...{ ref }} />
        </MyRectangle>
      
        <p>Szerokość: {size.width} px</p>
        <p>Wysokość: {size.height} px</p>
        <button onClick={() => handleResize()}>Generuj nowy element</button>
      </div>
    );
  }
 
export default Rectangle;